# Kaniko container image build test

Test building OCI/docker images with [kaniko](https://github.com/GoogleContainerTools/kaniko),
without docker-in-docker or any need of a privileged container.

See also the [gitlab CI instructions for kaniko](https://docs.gitlab.com/ce/ci/docker/using_kaniko.html).

See [kaniko in action in this project's gitlab CI job](https://0xacab.org/infrared/platform_wg/kaniko-build-test/-/jobs/97516).

Test the resulting image:

    $ docker run --rm -it registry.0xacab.org/infrared/platform_wg/kaniko-build-test
    It works !


## Alternatives

A small overview about similar alternatives for building OCI/docker images can be found
in the [Comparison with Other Tools section of  the Kaniko README.md](https://github.com/GoogleContainerTools/kaniko#comparison-with-other-tools).

Some more comments about potential options how to build without a privileged container:

- [buildah *still* needs a privileged conatiner](https://github.com/containers/buildah/issues/1335),
  see also https://medium.com/prgcont/using-buildah-in-gitlab-ci-9b529af19e42.
